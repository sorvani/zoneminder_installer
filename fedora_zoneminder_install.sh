#!/bin/bash

zoneminder_install()
{
    # Enable access to free and nonfree RPM Fusion repo
    sudo dnf -y install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm \
        https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

    # Install Zoneminder
    sudo dnf -y install zoneminder

    # Configure MariaDB server
    sudo dnf -y install mariadb-server
    sudo systemctl enable --now mariadb
    
    export DB_ZMUSER_PASS="$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 30)"
    
    mysql -u root < /usr/share/zoneminder/db/zm_create.sql
    sudo mysql -e "GRANT ALL ON zm.* TO 'zmuser'@'localhost' identified by '$DB_ZMUSER_PASS';"
    sudo mysql -e "FLUSH PRIVILEGES;"

    # Secure MariaDB
    export DB_ROOT_PASS="$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 30)"
    
    sudo mysql -e "UPDATE mysql.user SET Password=PASSWORD('$DB_ROOT_PASS') WHERE User='root';"
    sudo mysql -e "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');"
    sudo mysql -e "DELETE FROM mysql.user WHERE User='';"
    sudo mysql -e "FLUSH PRIVILEGES;"

    {
        echo "ZM_DB_USER = zmuser"
        echo "ZM_DB_PASS = $DB_ZMUSER_PASS"
    } | sudo tee --append /etc/zm/conf.d/zm-db-user.conf > /dev/null

    sudo chown root:apache /etc/zm/conf.d/*.conf
    sudo chmod 640 /etc/zm/conf.d/*.conf

    timezone=$(timedatectl | grep "Time zone:" | \
        awk -F':' '{ print $2 }' | awk '{ print $1 }')
    sudo sed -e "s:^;date.timezone =.*:date.timezone = \"${timezone}\":g" -i "/etc/php.ini"

    sudo setenforce 0

    sudo sed -i 's/^SELINUX=.*/SELINUX=permissive/g' /etc/selinux/config

    sudo ln -sf /etc/zm/www/zoneminder.httpd.conf /etc/httpd/conf.d/

    sudo dnf -y install mod_ssl

    # Start Web server
    sudo systemctl enable --now httpd
    
    # Start ZoneMinder
    sudo systemctl enable --now zoneminder
    
    # Configure firewall
    sudo firewall-cmd --permanent --add-service=http
    sudo firewall-cmd --permanent --add-service=https
    sudo firewall-cmd --permanent --add-port=3702/udp
    sudo firewall-cmd --reload
}

zoneminder_install

# Database Information
cat << EOF

MySql Database Name     : zm
Database User           : zmuser
Database User Password  : $DB_ZMUSER_PASS
Database Root Password  : $DB_ROOT_PASS

EOF

# Access ZoneMinder web console
cat << EOF

http://localhost/zm (works from the local machine only)
http://{machine name}/zm (works only if dns is configured for your network)
http://{ip address}/zm

EOF