# ZoneMinder Installer

Install script for ZoneMinder

This single line installation script works on a minimal install of Fedora Server 29.
You can either run the installer from a root shell or users that are members of the wheel group.
This group is already set up to provide sudo access.

```
curl https://gitlab.com/black3dynamite/zoneminder_installer/raw/master/fedora_zoneminder_install.sh | sudo bash
```
